package main

import (
	"fmt"
	"os"
)

func main() {
	dataBytes, err := os.ReadFile("input")
	if err != nil {
		fmt.Printf("Failed to read file with error: '%s'", err)
		os.Exit(1)
	}
	data := string(dataBytes)

	turn := Turn{reds: 12, greens: 13, blues: 14}
	inputGames := games(data)
	possibleGames := GamesPossible(inputGames, turn)
	idSum := IdSum(possibleGames)
	sumOfPowers := SumOfPowers(inputGames)

	fmt.Printf("Part 1: Sum of games IDs: '%d'\n", idSum)
	fmt.Printf("Part 2: Sum of powers   : '%d'\n", sumOfPowers)
}
