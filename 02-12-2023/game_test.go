package main

import (
	"reflect"
	"testing"
)

func TestGameParsing(t *testing.T) {
	var tests = []struct {
		input string
		want  Game
	}{
		{"Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green", Game{
			turns: []Turn{
				{blues: 3, reds: 4},
				{reds: 1, greens: 2, blues: 6},
				{greens: 2},
			},
			id: 1,
		}},
		{"Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue", Game{
			turns: []Turn{
				{blues: 1, greens: 2},
				{blues: 4, reds: 1, greens: 3},
				{blues: 1, greens: 1},
			},
			id: 2,
		}},
		{"Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red", Game{
			turns: []Turn{
				{greens: 8, blues: 6, reds: 20},
				{blues: 5, reds: 4, greens: 13},
				{greens: 5, reds: 1},
			},
			id: 3,
		}},
		{"Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red", Game{
			turns: []Turn{
				{greens: 1, blues: 6, reds: 3},
				{reds: 6, greens: 3},
				{reds: 14, blues: 15, greens: 3},
			},
			id: 4,
		}},
		{"Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green", Game{
			turns: []Turn{
				{blues: 1, reds: 6, greens: 3},
				{blues: 2, reds: 1, greens: 2},
			},
			id: 5,
		}},
		{"Game 100: 6 reds", Game{
			turns: []Turn{
				{reds: 6},
			},
			id: 100,
		}},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			actual := game(test.input)

			if !reflect.DeepEqual(actual, test.want) {
				t.Errorf("Got: '%+v' want '%+v'", actual, test.want)
			}
		})
	}
}

func TestGamesParse(t *testing.T) {
	input := "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\nGame 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\nGame 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\nGame 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\nGame 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"
	want := []Game{
		{
			turns: []Turn{
				{blues: 3, reds: 4},
				{reds: 1, greens: 2, blues: 6},
				{greens: 2},
			},
			id: 1,
		},
		{
			turns: []Turn{
				{blues: 1, greens: 2},
				{blues: 4, reds: 1, greens: 3},
				{blues: 1, greens: 1},
			},
			id: 2,
		},
		{
			turns: []Turn{
				{greens: 8, blues: 6, reds: 20},
				{blues: 5, reds: 4, greens: 13},
				{greens: 5, reds: 1},
			},
			id: 3,
		},
		{
			turns: []Turn{
				{greens: 1, blues: 6, reds: 3},
				{reds: 6, greens: 3},
				{reds: 14, blues: 15, greens: 3},
			},
			id: 4,
		},
		{
			turns: []Turn{
				{blues: 1, reds: 6, greens: 3},
				{blues: 2, reds: 1, greens: 2},
			},
			id: 5,
		},
	}

	actual := games(input)

	if !reflect.DeepEqual(actual, want) {
		t.Errorf("Got: '%+v' want '%+v'", actual, want)
	}
}

func TestGame_LeastRequiredTurn(t *testing.T) {
	var tests = []struct {
		input       Game
		want        Turn
		description string
	}{
		{
			input: Game{turns: []Turn{
				{reds: 1, blues: 1, greens: 1},
			}},
			want:        Turn{reds: 1, blues: 1, greens: 1},
			description: "Single turn non default values, should return those same values",
		},
		{
			input: Game{turns: []Turn{
				{reds: 3, blues: 1, greens: 1},
				{reds: 1, blues: 3, greens: 2},
				{reds: 2, blues: 2, greens: 3},
			}},
			want:        Turn{reds: 3, blues: 3, greens: 3},
			description: "Three turns each providing one max value, should return Turn with max of each",
		},
	}

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			actual := test.input.LeastRequiredTurn()

			if !reflect.DeepEqual(actual, test.want) {
				t.Errorf("Got: '%+v' want '%+v'", actual, test.want)
			}
		})
	}
}

func TestGamesPossible(t *testing.T) {
	var tests = []struct {
		description   string
		possibleGames []Game
		turn          Turn
		want          []Game
	}{
		{
			description: "Day 2 Part 1 example is covered",
			possibleGames: []Game{
				{turns: []Turn{
					{blues: 3, reds: 4},
					{reds: 1, greens: 2, blues: 6},
					{greens: 2},
				}},
				{turns: []Turn{
					{blues: 1, greens: 2},
					{blues: 4, reds: 1, greens: 3},
					{blues: 1, greens: 1},
				}},
				{turns: []Turn{
					{greens: 8, blues: 6, reds: 20},
					{blues: 5, reds: 4, greens: 13},
					{greens: 5, reds: 1},
				}},
				{turns: []Turn{
					{greens: 1, blues: 6, reds: 3},
					{reds: 6, greens: 3},
					{reds: 14, blues: 15, greens: 3},
				}},
				{turns: []Turn{
					{blues: 1, reds: 6, greens: 3},
					{blues: 2, reds: 1, greens: 2},
				}},
			},
			turn: Turn{reds: 12, greens: 13, blues: 14},
			want: []Game{
				{turns: []Turn{
					{blues: 3, reds: 4},
					{reds: 1, greens: 2, blues: 6},
					{greens: 2},
				}},
				{turns: []Turn{
					{blues: 1, greens: 2},
					{blues: 4, reds: 1, greens: 3},
					{blues: 1, greens: 1},
				}},
				{turns: []Turn{
					{blues: 1, reds: 6, greens: 3},
					{blues: 2, reds: 1, greens: 2},
				}},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			actual := GamesPossible(test.possibleGames, test.turn)

			if !reflect.DeepEqual(actual, test.want) {
				t.Errorf("\nGot     : '%+v'\nwant    : '%+v'\nturn    : '%+v'\npossible: '%+v'", actual, test.want, test.turn, test.possibleGames)
			}
		})
	}
}

func TestIdSum(t *testing.T) {
	var tests = []struct {
		description string
		input       []Game
		want        int
	}{
		{
			description: "Single game returns id of game",
			input: []Game{
				{id: 100},
			},
			want: 100,
		},
		{
			description: "Ids of 100, 1 and 2 should return 103",
			input: []Game{
				{id: 100},
				{id: 1},
				{id: 2},
			},
			want: 103,
		},
	}

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			actual := IdSum(test.input)

			if !reflect.DeepEqual(actual, test.want) {
				t.Errorf("\nGot  : '%+v'\nwant : '%+v'\ngames    : '%+v'", actual, test.want, test.input)
			}
		})
	}
}

func TestSumOfPowers(t *testing.T) {
	var tests = []struct {
		description string
		input       []Game
		want        int
	}{
		{
			description: "Part 2 test input",
			input: []Game{
				{
					id: 1,
					turns: []Turn{
						{blues: 3, reds: 4},
						{blues: 6, reds: 1, greens: 2},
						{greens: 2},
					},
				},
				{
					id: 2,
					turns: []Turn{
						{blues: 1, greens: 2},
						{greens: 3, blues: 4, reds: 1},
						{greens: 1, blues: 1},
					},
				},
				{
					id: 3,
					turns: []Turn{
						{greens: 8, blues: 6, reds: 20},
						{blues: 5, reds: 4, greens: 13},
						{greens: 5, reds: 1},
					},
				},
				{
					id: 4,
					turns: []Turn{
						{greens: 1, reds: 3, blues: 6},
						{greens: 3, reds: 6},
						{greens: 3, blues: 15, reds: 14},
					},
				},
				{
					id: 5,
					turns: []Turn{
						{reds: 6, blues: 1, greens: 3},
						{blues: 2, reds: 1, greens: 2},
					},
				},
			},
			want: 2286,
		},
	}

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			actual := SumOfPowers(test.input)

			if !reflect.DeepEqual(actual, test.want) {
				t.Errorf("\nGot  : '%+v'\nwant : '%+v'\ngames    : '%+v'", actual, test.want, test.input)
			}
		})
	}
}
