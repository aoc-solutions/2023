package main

import "testing"

func TestGetTurn(t *testing.T) {
	var tests = []struct {
		input string
		want  Turn
	}{
		{"3 blue, 4 red", Turn{blues: 3, reds: 4}},
		{"1 red, 2 green", Turn{reds: 1, greens: 2}},
		{"6 blue, 2 green", Turn{blues: 6, greens: 2}},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			actual := getTurn(test.input)

			if actual != test.want {
				t.Errorf("Got: '%+v' want '%+v'", actual, test.want)
			}
		})
	}
}

func TestTurn_TurnPossible(t *testing.T) {
	var tests = []struct {
		uut         Turn
		possible    Turn
		want        bool
		description string
	}{
		{
			uut:         Turn{reds: 10, blues: 10, greens: 10},
			possible:    Turn{reds: 9, blues: 3, greens: 1},
			want:        false,
			description: "When all turn values are less then it should not be possible",
		},
		{
			uut:         Turn{reds: 10, blues: 10, greens: 10},
			possible:    Turn{reds: 11, blues: 3, greens: 1},
			want:        false,
			description: "When all turn values are less except for reds then it should not be possible",
		},
		{
			uut:         Turn{reds: 10, blues: 10, greens: 10},
			possible:    Turn{reds: 2, blues: 12, greens: 1},
			want:        false,
			description: "When all turn values are less except for blues then it should not be possible",
		},
		{
			uut:         Turn{reds: 10, blues: 10, greens: 10},
			possible:    Turn{reds: 9, blues: 3, greens: 20},
			want:        false,
			description: "When all turn values are less except for greens then it should not be possible",
		},
		{
			uut:         Turn{reds: 10, blues: 10, greens: 10},
			possible:    Turn{reds: 19, blues: 3, greens: 20},
			want:        false,
			description: "When all turn values are less except for red and greens then it should not be possible",
		},
		{
			uut:         Turn{reds: 10, blues: 10, greens: 10},
			possible:    Turn{reds: 19, blues: 23, greens: 0},
			want:        false,
			description: "When all turn values are less except for red and blues then it should not be possible",
		},
		{
			uut:         Turn{reds: 10, blues: 10, greens: 10},
			possible:    Turn{reds: 9, blues: 23, greens: 200},
			want:        false,
			description: "When all turn values are less except for blues and greens then it should not be possible",
		},
		{
			uut:         Turn{reds: 10, blues: 10, greens: 10},
			possible:    Turn{reds: 19, blues: 23, greens: 20},
			want:        true,
			description: "When all turn values are more it should be possible",
		},
		{
			uut:         Turn{reds: 10, blues: 10, greens: 10},
			possible:    Turn{reds: 10, blues: 10, greens: 10},
			want:        true,
			description: "When all turn values are equal it should be possible",
		},
	}

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			actual := test.uut.TurnPossible(test.possible)

			if actual != test.want {
				t.Errorf("\nGot     : '%+v'\nwant    : '%+v'\nuut     : '%+v'\npossible: '%+v'", actual, test.want, test.uut, test.possible)
			}
		})
	}
}

func TestTurn_Power(t *testing.T) {
	var tests = []struct {
		description string
		uut         Turn
		want        int
	}{
		{
			description: "Power with all zeros is zero",
			uut:         Turn{},
			want:        0,
		},
		{
			description: "Power where reds are zero is zero",
			uut:         Turn{reds: 0, greens: 1, blues: 1},
			want:        0,
		},
		{
			description: "Power where greens are zero is zero",
			uut:         Turn{reds: 1, greens: 0, blues: 1},
			want:        0,
		},
		{
			description: "Power where blues are zero is zero",
			uut:         Turn{reds: 1, greens: 1, blues: 0},
			want:        0,
		},
		{
			description: "Power where all are one is one",
			uut:         Turn{reds: 1, greens: 1, blues: 1},
			want:        1,
		},
		{
			description: "Power of 1,2,3 is 6",
			uut:         Turn{reds: 1, greens: 2, blues: 3},
			want:        6,
		},
	}

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			actual := test.uut.Power()

			if actual != test.want {
				t.Errorf("\nGot : '%+v'\nwant: '%+v'\nuut : '%+v'", actual, test.want, test.uut)
			}
		})
	}
}
