package main

import (
	"log"
	"regexp"
	"strconv"
	"strings"
)

type Turn struct {
	reds   int
	blues  int
	greens int
}

func (turn *Turn) addAmount(colorAmount string, amount int) {
	colorRegex := regexp.MustCompile("red|green|blue")
	color := colorRegex.FindString(colorAmount)

	switch color {
	case "red":
		turn.reds = amount
	case "green":
		turn.greens = amount
	case "blue":
		turn.blues = amount
	default:
		log.Fatalf("Failed to find color in string '%s'", colorAmount)
	}
}

func getTurn(line string) Turn {
	valueRegex := regexp.MustCompile("\\d+")
	colors := strings.Split(line, ",")
	var turn Turn

	for _, colorAmount := range colors {
		amountStr := valueRegex.FindString(colorAmount)
		amount, err := strconv.Atoi(amountStr)

		if err != nil {
			log.Fatalf("Failed to parse '%s' as integer", amountStr)
		}

		turn.addAmount(colorAmount, amount)
	}

	return turn
}

func (turn Turn) TurnPossible(input Turn) bool {
	return turn.reds <= input.reds && turn.blues <= input.blues && turn.greens <= input.greens
}

func (turn Turn) Power() int {
	return turn.reds * turn.greens * turn.blues
}
