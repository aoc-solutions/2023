package main

import (
	"log"
	"regexp"
	"strconv"
	"strings"
)

type Game struct {
	id    int
	turns []Turn
}

// LeastRequiredTurn gather all values from all turns and take the maximum value of each color and returns a Turn
// with the highest value of each
func (game Game) LeastRequiredTurn() Turn {
	var res Turn

	for _, turn := range game.turns {
		if res.reds < turn.reds {
			res.reds = turn.reds
		}
		if res.blues < turn.blues {
			res.blues = turn.blues
		}
		if res.greens < turn.greens {
			res.greens = turn.greens
		}
	}

	return res
}

func game(line string) Game {
	idRegex, _ := regexp.Compile("\\d+")
	gameLine := strings.Split(line, ": ")
	turnsLine := gameLine[1]
	idLine := gameLine[0]
	turnsStr := strings.Split(turnsLine, ";")
	var turns []Turn

	for _, turnValues := range turnsStr {
		turns = append(turns, getTurn(turnValues))
	}
	idStr := idRegex.FindString(idLine)
	id, err := strconv.Atoi(idStr)
	if err != nil {
		log.Fatalf("Failed to find the ID of string '%s', cannot continue", idLine)
	}

	return Game{
		turns: turns,
		id:    id,
	}
}

func games(input string) []Game {
	lines := strings.Split(input, "\n")
	var res []Game

	for _, line := range lines {
		if line == "" {
			continue
		}

		res = append(res, game(line))
	}

	return res
}

func (game Game) turnPossible(turn Turn) bool {
	return game.LeastRequiredTurn().TurnPossible(turn)
}

func GamesPossible(games []Game, turn Turn) []Game {
	var res []Game

	for _, g := range games {
		if g.turnPossible(turn) {
			res = append(res, g)
		}
	}

	return res
}

func IdSum(games []Game) int {
	res := 0

	for _, g := range games {
		res += g.id
	}

	return res
}

func (game Game) power() int {
	return game.LeastRequiredTurn().Power()
}

func SumOfPowers(games []Game) int {
	res := 0

	for _, g := range games {
		res += g.power()
	}

	return res
}
