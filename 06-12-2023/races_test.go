package main

import "testing"

func TestRaces_Factor(t *testing.T) {
	tests := []struct {
		name  string
		races Races
		want  int
	}{
		{
			name: "Know test input day 1",
			races: Races{
				{7, 9},
				{15, 40},
				{30, 200},
			},
			want: 288,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.races.Factor(); got != tt.want {
				t.Errorf("Factor() = %v, want %v", got, tt.want)
			}
		})
	}
}
