package main

import (
	"reflect"
	"testing"
)

func TestRace_winningTime(t *testing.T) {
	tests := []struct {
		name     string
		race     Race
		timeHeld int
		want     bool
	}{
		{
			name: "Known first winning combo from example input",
			race: Race{
				time:     7,
				distance: 9,
			},
			timeHeld: 2,
			want:     true,
		},
		{
			name: "Known last winning combo from example input",
			race: Race{
				time:     7,
				distance: 9,
			},
			timeHeld: 5,
			want:     true,
		},
		{
			name: "Known first failing combo from example input",
			race: Race{
				time:     7,
				distance: 9,
			},
			timeHeld: 1,
			want:     false,
		},
		{
			name: "Day 2 known false positive",
			race: Race{
				time:     59707878,
				distance: 430121812131276,
			},
			timeHeld: 430121812131276,
			want:     false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.race.winningTime(tt.timeHeld); got != tt.want {
				t.Errorf("winningTime() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRace_WinningRange(t *testing.T) {
	tests := []struct {
		name string
		race Race
		want Range
	}{
		{
			name: "Known first winning range from example input day 1",
			race: Race{
				time:     7,
				distance: 9,
			},
			want: Range{
				first:  2,
				amount: 4,
			},
		}, {
			name: "Known first winning range from example input day 2",
			race: Race{
				time:     71530,
				distance: 940200,
			},
			want: Range{
				first:  14,
				amount: 71503,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.race.WinningRange(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("WinningRange() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRace_FirstWinning(t *testing.T) {
	tests := []struct {
		name string
		race Race
		want int
	}{
		{
			name: "Known first winning time",
			race: Race{
				time:     7,
				distance: 9,
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.race.FirstWinning(); got != tt.want {
				t.Errorf("FirstWinning() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRace_LastWinning(t *testing.T) {
	tests := []struct {
		name string
		race Race
		want int
	}{
		{
			name: "know last winning number from test input",
			race: Race{
				time:     7,
				distance: 9,
			},
			want: 5,
		},
		{
			name: "Known failing case",
			race: Race{
				time:     70,
				distance: 1218,
			},
			want: 37,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.race.LastWinning(); got != tt.want {
				t.Errorf("LastWinning() = %v, want %v", got, tt.want)
			}
		})
	}
}
