package main

type Races []Race

func (races Races) Factor() int {
	var result int

	for i, race := range races {
		if i == 0 {
			result = race.WinningRange().amount
		} else {
			result = result * race.WinningRange().amount
		}
	}

	return result
}
