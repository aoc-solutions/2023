package main

import (
	"reflect"
	"testing"
)

func Test_toRaceDay2(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name string
		args args
		want Race
	}{
		{
			name: "day 2 test input",
			args: args{
				input: "Time:      7  15   30\nDistance:  9  40  200",
			},
			want: Race{
				time:     71530,
				distance: 940200,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := toRaceDay2(tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("toRaceDay2() = %v, want %v", got, tt.want)
			}
		})
	}
}
