package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Range struct {
	first  int
	amount int
}

func toRaces(input string) Races {
	timeLineRegex := regexp.MustCompile(`(?m)^Time:.*`)
	distanceLineRegex := regexp.MustCompile(`(?m)^Distance:.*`)
	numbersRegex := regexp.MustCompile(`\d+`)
	var result Races

	timeValues := numbersRegex.FindAllString(timeLineRegex.FindString(input), -1)
	distanceValues := numbersRegex.FindAllString(distanceLineRegex.FindString(input), -1)

	for i := 0; i < len(timeValues); i++ {
		time, _ := strconv.Atoi(timeValues[i])
		distance, _ := strconv.Atoi(distanceValues[i])
		result = append(result, Race{
			time:     time,
			distance: distance,
		})
	}

	return result
}

func toRaceDay2(input string) Race {
	timeLineRegex := regexp.MustCompile(`(?m)^Time:.*`)
	distanceLineRegex := regexp.MustCompile(`(?m)^Distance:.*`)
	numbersRegex := regexp.MustCompile(`\d+`)

	timeValue, _ := strconv.Atoi(strings.Join(numbersRegex.FindAllString(timeLineRegex.FindString(input), -1), ""))
	distanceValue, _ := strconv.Atoi(strings.Join(numbersRegex.FindAllString(distanceLineRegex.FindString(input), -1), ""))

	return Race{
		time:     timeValue,
		distance: distanceValue,
	}
}

func main() {
	inputBytes, err := os.ReadFile("input")
	if err != nil {
		fmt.Printf("Failed to read file with error '%s'", err)
		os.Exit(1)
	}
	input := string(inputBytes)
	races := toRaces(input)
	day1factor := races.Factor()
	println("Day 1 factor:", day1factor)
	day2Race := toRaceDay2(input)
	day2Winning := day2Race.WinningRange()
	println("Day 2 Winning:", day2Winning.amount)
}
