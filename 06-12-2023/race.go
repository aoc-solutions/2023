package main

import "math"

type Race struct {
	time     int
	distance int
}

func (race Race) winningTime(timeHeld int) bool {
	timeLeft := race.time - timeHeld
	distanceMoved := timeLeft * timeHeld
	return timeLeft > 0 && distanceMoved > race.distance
}

func (race Race) WinningRange() Range {
	firstWinning := race.FirstWinning()
	lastWinning := race.LastWinning()

	return Range{
		first:  firstWinning,
		amount: lastWinning - firstWinning + 1,
	}
}

func (race Race) FirstWinning() int {
	result := -1

	for i := 1.0; result == -1; i++ {
		sqrtDistance := int(math.Sqrt(float64(race.distance)) / i)
		for j := 0; j < race.distance; j += sqrtDistance {
			if race.winningTime(j) {
				result = j
				break
			}
		}
	}

	for i := result; i > 0; i-- {
		if !race.winningTime(i) {
			result = i + 1
			break
		}
	}

	return result
}

func (race Race) LastWinning() int {
	result := -1

	for i := 1.0; result == -1; i++ {
		sqrtDistance := int(math.Sqrt(float64(race.distance)) / i)
		for j := race.distance; j > 0; j -= sqrtDistance {
			if race.winningTime(j) {
				result = j
				break
			}
		}
	}

	for i := result; i < race.distance; i++ {
		if !race.winningTime(i) {
			result = i - 1
			break
		}
	}

	return result
}
