package main

import (
	"math"
	"reflect"
	"testing"
)

func Test_parseMap(t *testing.T) {
	tests := []struct {
		name  string
		lines string
		want  DestinationMaps
	}{
		{
			name:  "Can parse single line",
			lines: "50 51 2",
			want: DestinationMaps{
				{Dest: 50, Source: 51, Amount: 2},
			},
		},
		{
			name:  "Can parse double line",
			lines: "50 51 2\n90 112 5",
			want: DestinationMaps{
				{Dest: 50, Source: 51, Amount: 2},
				{Dest: 90, Source: 112, Amount: 5},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseMap(tt.lines); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDestinationMap_GetDestination(t *testing.T) {
	type args struct {
		input       int
		destination DestinationMap
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Return destination from map where input is within required range",
			args: args{
				input: 50,
				destination: DestinationMap{
					Dest:   312,
					Source: 40,
					Amount: 20,
				},
			},
			want: 322,
		},
		{
			name: "Returns -1 when input is outside bounds",
			args: args{
				input: 100,
				destination: DestinationMap{
					Dest:   1,
					Source: 1,
					Amount: 10,
				},
			},
			want: -1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := tt.args.destination.GetDestination(tt.args.input); got != tt.want {
				t.Errorf("GetDestination() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDestinationMaps_GetDestination(t *testing.T) {
	tests := []struct {
		name            string
		destinationMaps DestinationMaps
		input           int
		want            int
		wantErr         bool
	}{
		{
			name: "Can get destination from mapped value 79",
			destinationMaps: DestinationMaps{
				{Dest: 50, Source: 98, Amount: 2},
				{Dest: 52, Source: 50, Amount: 48},
			},
			input:   79,
			want:    81,
			wantErr: false,
		},
		{
			name: "Can get destination from unmapped value 81",
			destinationMaps: DestinationMaps{
				{Dest: 0, Source: 15, Amount: 37},
				{Dest: 37, Source: 52, Amount: 2},
				{Dest: 39, Source: 0, Amount: 15},
			},
			input:   81,
			want:    81,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.destinationMaps.GetDestination(tt.input)
			if got != tt.want {
				t.Errorf("GetDestination() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDestinationMap_GetSource(t *testing.T) {
	tests := []struct {
		name        string
		destination DestinationMap
		input       int
		want        int
		wantErr     bool
	}{
		{
			name: "Return destination from map where input is within required range",
			destination: DestinationMap{
				Dest:   312,
				Source: 40,
				Amount: 20,
			},
			input:   322,
			want:    50,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.destination.GetSource(tt.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetSource() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetSource() got = %v, want %v", got, tt.want)
			}
		})
	}
}

//func TestDestinationMaps_GetSource(t *testing.T) {
//	tests := []struct {
//		name            string
//		destinationMaps DestinationMaps
//		input           int
//		want            int
//	}{
//		{
//			name: "Can get seed 82 from location 46 as per example part two",
//			destinationMaps:
//		},
//	}
//	for _, tt := range tests {
//		t.Run(tt.name, func(t *testing.T) {
//			if got := tt.destinationMaps.GetSource(tt.input); got != tt.want {
//				t.Errorf("GetSource() = %v, want %v", got, tt.want)
//			}
//		})
//	}
//}

func TestDestinationMaps_Destinations(t *testing.T) {
	type args struct {
		source int
		amount int
	}
	tests := []struct {
		name            string
		destinationMaps DestinationMaps
		args            args
		want            DestinationMaps
	}{
		{
			name: "Range fitting perfectly in maps, one level",
			destinationMaps: []DestinationMap{
				{2, 1, 10},
			},
			args: args{source: 1, amount: 10},
			want: DestinationMaps{
				{2, 1, 10},
			},
		},
		{
			name: "Range outside of maps left, input equal output",
			destinationMaps: []DestinationMap{
				{2, 11, 10},
			},
			args: args{source: 1, amount: 2},
			want: DestinationMaps{
				{1, 1, 2},
			},
		},
		{
			name: "Range outside of maps right, input equal output",
			destinationMaps: []DestinationMap{
				{2, 11, 10},
			},
			args: args{source: 22, amount: 2},
			want: DestinationMaps{
				{22, 22, 2},
			},
		},
		{
			name: "Range between of map, input equal output",
			destinationMaps: []DestinationMap{
				{2, 11, 10},
				{22, 31, 10},
			},
			args: args{source: 21, amount: 10},
			want: DestinationMaps{
				{21, 21, 10},
			},
		},
		{
			name: "Range split between two maps",
			destinationMaps: []DestinationMap{
				{2, 11, 10},
				{22, 21, 10},
			},
			args: args{source: 16, amount: 10},
			want: DestinationMaps{
				{7, 16, 5},
				{22, 21, 5},
			},
		},
		{
			name: "Range split between two maps, with empty range in middle",
			destinationMaps: []DestinationMap{
				{2, 11, 10},
				{42, 31, 10},
			},
			args: args{source: 16, amount: 20},
			want: DestinationMaps{
				{7, 16, 5},
				{21, 21, 10},
				{42, 31, 5},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.destinationMaps.Destinations(tt.args.source, tt.args.amount); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Destinations() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDestinationMaps_fillGaps(t *testing.T) {
	tests := []struct {
		name            string
		destinationMaps DestinationMaps
		want            DestinationMaps
	}{
		{
			name: "input starting at 0, with 10 in range, can add range from 10 to MaxInt",
			destinationMaps: DestinationMaps{
				{2, 0, 10},
			},
			want: DestinationMaps{
				{2, 0, 10},
				{10, 10, math.MaxInt - 10},
			},
		}, {
			name: "input starting at 10, with 10 in range, can add range from 0 to 9 before and 20 to MaxInt after",
			destinationMaps: DestinationMaps{
				{2, 10, 10},
			},
			want: DestinationMaps{
				{0, 0, 10},
				{2, 10, 10},
				{20, 20, math.MaxInt - 20},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.destinationMaps.fillGaps(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("fillGaps() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_minDestination(t *testing.T) {
	type args struct {
		maps  []DestinationMaps
		seeds []int
	}
	maps := parseMaps(exampleInput)
	seeds := parseSeeds(exampleInput)
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Example input",
			args: args{
				maps:  maps,
				seeds: seeds,
			},
			want: 46,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := minDestination(tt.args.maps, tt.args.seeds); got != tt.want {
				t.Errorf("minDestination() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEndDestinations(t *testing.T) {
	type args struct {
		maps   []DestinationMaps
		index  int
		source int
		amount int
	}

	maps := parseMaps(exampleInput)

	tests := []struct {
		name string
		args args
		want DestinationMaps
	}{
		{
			name: "test end destination for test input",
			args: args{
				maps:   maps,
				index:  0,
				source: 82,
				amount: 1,
			},
			want: DestinationMaps{
				{Dest: 46, Source: 46, Amount: 1},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EndDestinations(tt.args.maps, tt.args.index, tt.args.source, tt.args.amount); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EndDestinations() = %v, want %v", got, tt.want)
			}
		})
	}
}
