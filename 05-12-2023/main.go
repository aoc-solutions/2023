package main

import (
	"fmt"
	"log"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func parseSeeds(input string) []int {
	seedsLineRegex := regexp.MustCompile("(?m)^seeds:.*$")
	seedsRegex := regexp.MustCompile("\\d+")
	res := seedsLineRegex.FindString(input)
	seeds := seedsRegex.FindAllString(res, -1)
	var result []int

	for _, seed := range seeds {
		seedInt, _ := strconv.Atoi(seed)
		result = append(result, seedInt)
	}

	return result
}

func parseMaps(input string) []DestinationMaps {
	result := make([]DestinationMaps, 7)
	var mapNames = [7]string{"seed-to-soil", "soil-to-fertilizer", "fertilizer-to-water", "water-to-light", "light-to-temperature", "temperature-to-humidity", "humidity-to-location"}
	sections := strings.Split(input, "\n\n")

	for i, mapName := range mapNames {
		regex := regexp.MustCompile("(?m)^" + mapName + ".*$")
		var mapSection string
		var found = false

		for _, section := range sections {
			if regex.MatchString(section) {
				mapSection = section
				found = true
				break
			}
		}
		if !found {
			log.Fatalf("Failed to parse line: %s, expected a map named \"%s\"", input, mapName)
		}

		result[i] = parseMap(mapSection)
	}

	return result
}

func GetSeedDestination(seed int, maps []DestinationMaps) int {
	destination := seed

	for _, destinationMaps := range maps {
		destination = destinationMaps.GetDestination(destination)
	}

	return destination
}

func GetClosestPath(input string) int {
	maps := parseMaps(input)
	seeds := parseSeeds(input)
	lowestDestination := math.MaxInt

	for _, seed := range seeds {
		seedDestination := GetSeedDestination(seed, maps)
		if seedDestination < lowestDestination {
			lowestDestination = seedDestination
		}
	}

	return lowestDestination
}

func InSeedRange(value int, seeds []int) bool {
	for i := 0; i < len(seeds); i += 2 {
		lower := seeds[i]
		upper := lower + seeds[i+1]
		if value >= lower && value < upper {
			return true
		}
	}
	return false
}

func GetClosestPathPart2(input string) int {
	maps := parseMaps(input)
	seeds := parseSeeds(input)
	for location := 0; ; location++ {
		seed := location

		for i := len(maps) - 1; i >= 0; i-- {
			seed = maps[i].GetSource(seed)
		}

		if InSeedRange(seed, seeds) {
			return location
		}
	}
}

func GetClosestPathPart2_try2(input string) int {
	maps := parseMaps(input)
	seeds := parseSeeds(input)
	return minDestination(maps, seeds)
}

func main() {
	inputBytes, err := os.ReadFile("input")
	if err != nil {
		fmt.Printf("Failed to read file with error: '%s'", err)
		os.Exit(1)
	}
	input := string(inputBytes)
	closestPath := GetClosestPath(input)
	println(closestPath)
	//closestPathPart2 := GetClosestPathPart2(input)
	closestPathPart2 := GetClosestPathPart2_try2(input)
	println(closestPathPart2)
}
