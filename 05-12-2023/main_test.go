package main

import (
	"reflect"
	"testing"
)

func Test_parseSeeds(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  []int
	}{
		{
			name:  "Can parse single line",
			input: "seeds: 79 14 55 13\n",
			want:  []int{79, 14, 55, 13},
		},
		{
			name:  "Can parse multi line",
			input: "seeds: 9 14 5 3\nblah sadfjsadlfj\n 123 2 231 13",
			want:  []int{9, 14, 5, 3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := parseSeeds(tt.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseSeeds() = %v, want %v", got, tt.want)
			}
		})
	}
}

const exampleInput = "seeds: 79 14 55 13\n\nseed-to-soil map:\n50 98 2\n52 50 48\n\nsoil-to-fertilizer map:\n0 15 37\n37 52 2\n39 0 15\n\nfertilizer-to-water map:\n49 53 8\n0 11 42\n42 0 7\n57 7 4\n\nwater-to-light map:\n88 18 7\n18 25 70\n\nlight-to-temperature map:\n45 77 23\n81 45 19\n68 64 13\n\ntemperature-to-humidity map:\n0 69 1\n1 0 69\n\nhumidity-to-location map:\n60 56 37\n56 93 4"

func TestGetClosestPath(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  int
	}{
		{
			name:  "Can get closest path from example input",
			input: exampleInput,
			want:  35,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetClosestPath(tt.input); got != tt.want {
				t.Errorf("GetClosestPath() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetClosestPathPart2(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  int
	}{
		{
			name:  "Can get closest path from example input",
			input: exampleInput,
			want:  46,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetClosestPathPart2(tt.input); got != tt.want {
				t.Errorf("GetClosestPathPart2() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestInSeedRange(t *testing.T) {
	exampleSeeds := []int{79, 14, 55, 13}
	type args struct {
		value int
		seeds []int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "Seed first in range is true",
			args: args{
				value: 79,
				seeds: exampleSeeds,
			},
			want: true,
		},
		{
			name: "Seed last in range is true",
			args: args{
				value: 92,
				seeds: exampleSeeds,
			},
			want: true,
		},
		{
			name: "Seed after last in range is false",
			args: args{
				value: 93,
				seeds: exampleSeeds,
			},
			want: false,
		},
		{
			name: "Seed second line in the middle of range is true",
			args: args{
				value: 63,
				seeds: exampleSeeds,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InSeedRange(tt.args.value, tt.args.seeds); got != tt.want {
				t.Errorf("InSeedRange() = %v, want %v", got, tt.want)
			}
		})
	}
}
