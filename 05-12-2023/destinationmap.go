package main

import (
	"fmt"
	"log"
	"math"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

type DestinationMap struct {
	Dest   int
	Source int
	Amount int
}

func (destination DestinationMap) GetDestination(input int) (int, error) {
	if (input >= destination.Source) && (input < destination.Source+destination.Amount) {
		offset := input - destination.Source
		return destination.Dest + offset, nil
	}

	return input, fmt.Errorf("destination out of range")
}

type DestinationMaps []DestinationMap

func (destinationMaps DestinationMaps) GetDestination(input int) int {
	for _, destinationMap := range destinationMaps {
		destination, err := destinationMap.GetDestination(input)
		if err == nil {
			return destination
		}
	}

	return input
}

func (destination DestinationMap) GetSource(input int) (int, error) {
	if (input >= destination.Dest) && (input < destination.Dest+destination.Amount) {
		offset := input - destination.Dest
		return destination.Source + offset, nil
	}

	return input, fmt.Errorf("source out of range")
}

func (destinationMaps DestinationMaps) GetSource(input int) int {
	for _, destinationMap := range destinationMaps {
		source, err := destinationMap.GetSource(input)
		if err == nil {
			return source
		}
	}

	return input
}

func parseMap(input string) DestinationMaps {
	regex := regexp.MustCompile("\\d+")
	var result DestinationMaps
	lines := strings.Split(input, "\n")

	for _, line := range lines {
		matches := regex.FindAllString(line, -1)
		amountOfMatches := len(matches)

		if amountOfMatches == 0 {
			continue
		} else if amountOfMatches != 3 {
			log.Fatalf("Failed to parse line: %s, expected 3 results but got: '%d'", line, amountOfMatches)
		}

		// We can ignore the potential error due to only finding integers with the regex - famous last words..
		destStart, _ := strconv.Atoi(matches[0])
		sourceStart, _ := strconv.Atoi(matches[1])
		genAmount, _ := strconv.Atoi(matches[2])

		result = append(result, DestinationMap{destStart, sourceStart, genAmount})
	}

	return result
}

func (destinationMap DestinationMap) DestinationRange(source, amount int) (int, error) {
	outputSourceEnd := destinationMap.Source + destinationMap.Amount
	inputSourceEnd := source + amount

	if source < destinationMap.Source || inputSourceEnd > outputSourceEnd {
		return -1, fmt.Errorf("source out of range, source can start from '%d' and end at '%d', not start at '%d' and end at '%d'", destinationMap.Source, outputSourceEnd, source, inputSourceEnd)
	}

	destination, err := destinationMap.GetDestination(source)

	if err != nil {
		return -1, err
	}

	return destination, nil
}

func (destinationMaps DestinationMaps) fillGaps() DestinationMaps {
	sort.Slice(destinationMaps[:], func(i, j int) bool { return destinationMaps[i].Source < destinationMaps[j].Source })
	var result DestinationMaps
	currentSource := 0

	for _, destinationMap := range destinationMaps {
		if destinationMap.Source > currentSource {
			// fill in the blank
			amount := destinationMap.Source - currentSource
			result = append(result, DestinationMap{currentSource, currentSource, amount})
			currentSource = currentSource + amount
		}
		result = append(result, destinationMap)
		currentSource = destinationMap.Source
	}

	restStart := destinationMaps[0].Source + destinationMaps[0].Amount
	return append(result, DestinationMap{
		Dest:   restStart,
		Source: restStart,
		Amount: math.MaxInt - restStart,
	})
}

func (destinationMaps DestinationMaps) Destinations(source, amount int) DestinationMaps {
	// start with sorted destinationmaps by inputsource
	inputSourceMax := source + amount
	var result DestinationMaps

	for _, destinationMap := range destinationMaps.fillGaps() {
		if inputSourceMax < destinationMap.Source {
			continue
		}

		if source >= destinationMap.Source {
			offset := source - destinationMap.Source
			outputSourceMax := destinationMap.Source + destinationMap.Amount
			nextDestinationMap := DestinationMap{Dest: destinationMap.Dest + offset, Source: source}

			if inputSourceMax <= outputSourceMax {
				nextDestinationMap.Amount = amount
				return append(result, nextDestinationMap)
			}

			possibleAmount := outputSourceMax - source
			if possibleAmount > 0 {
				nextDestinationMap.Amount = possibleAmount
				result = append(result, nextDestinationMap)
				source = source + possibleAmount
				amount = amount - possibleAmount
			}
			continue
		}
		sourceEqInputAmount := destinationMap.Source - source
		remainingAmount := amount - sourceEqInputAmount

		if sourceEqInputAmount > 0 {
			result = append(result, DestinationMap{Dest: source, Source: source, Amount: sourceEqInputAmount})
		}
		if remainingAmount == 0 {
			break
		}
		if remainingAmount > destinationMap.Amount {
			// we want more than available
			result = append(result, destinationMap)
			source += destinationMap.Amount
			amount = remainingAmount - destinationMap.Amount
		} else {
			return append(result, DestinationMap{Dest: destinationMap.Dest, Source: destinationMap.Source, Amount: remainingAmount})
		}
	}

	return result
}

func EndDestinations(maps []DestinationMaps, index, source, amount int) DestinationMaps {
	thisMap := maps[index]

	if len(maps)-1 == index {
		return thisMap.Destinations(source, amount)
	}

	var result DestinationMaps

	for _, destinationMap := range thisMap.Destinations(source, amount) {
		result = append(result, EndDestinations(maps, index+1, destinationMap.Dest, destinationMap.Amount)...)
	}

	return result
}

func minDestination(maps []DestinationMaps, seeds []int) int {
	res := math.MaxInt

	for i := 0; i < len(seeds); i += 2 {
		destinations := EndDestinations(maps, 0, seeds[i], seeds[i+1])
		for _, destination := range destinations {
			if destination.Dest < res {
				res = destination.Dest
			}
		}
	}

	return res
}
