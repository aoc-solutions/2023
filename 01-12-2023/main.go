package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func letterToNumber(number string) string {
	stringsToVaules := map[string]string{
		"one":   "1",
		"two":   "2",
		"three": "3",
		"four":  "4",
		"five":  "5",
		"six":   "6",
		"seven": "7",
		"eight": "8",
		"nine":  "9",
	}
	res := number
	val, exists := stringsToVaules[number]

	if exists {
		res = val
	}

	return res
}

func calibrationValue(input string) int {
	firstIndex := -1
	firstValue := ""
	lastIndex := -1
	lastValue := ""
	options := []string{"1", "2", "3", "4", "5", "6", "7", "8", "9", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}

	for _, option := range options {
		firstIndexOption := strings.Index(input, option)

		if firstIndexOption > -1 {
			if firstIndex == -1 || firstIndexOption < firstIndex {
				firstIndex = firstIndexOption
				firstValue = option
			}
		}

		lastIndexOption := strings.LastIndex(input, option)

		if lastIndexOption > -1 {
			if lastIndex == -1 || lastIndexOption > lastIndex {
				lastIndex = lastIndexOption
				lastValue = option
			}
		}
	}

	firstValue = letterToNumber(firstValue)
	lastValue = letterToNumber(lastValue)
	number := firstValue + lastValue
	res, err := strconv.Atoi(number)

	if err != nil {
		fmt.Printf("Failed to convert '%s' to integer.", number)
		os.Exit(1)
	}

	return res
}

func main() {
	data_bytes, err := os.ReadFile("input")
	if err != nil {
		fmt.Printf("Failed to read file with error: '%s'", err)
		os.Exit(1)
	}
	data := string(data_bytes)

	lines := strings.Split(data, "\n")
	totalCount := 0
	for _, element := range lines {
		if element == "" {
			continue
		}
		totalCount += calibrationValue(element)
	}

	fmt.Printf("Total Count: '%d'", totalCount)
}
