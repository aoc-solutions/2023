package main

import (
	"reflect"
	"testing"
)

func Test_GetSchematic(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  Schematic
	}{
		{
			name:  "Day 3 Part 1 input",
			input: "467..114..\n...*......\n..35..633.\n......#...\n617*......\n.....+.58.\n..592.....\n......755.\n...$.*....\n.664.598..",
			want: Schematic{
				partNumbers: []int{467, 35, 633, 617, 592, 755, 664, 598},
				gearRatios:  []int{16345, 451490},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetSchematic(tt.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("\ngot : %v\nwant: %v", got, tt.want)
			}
		})
	}
}

func Test_GetSchematic_SumOfPartNumbers(t *testing.T) {
	want := 4361
	schematic := GetSchematic("467..114..\n...*......\n..35..633.\n......#...\n617*......\n.....+.58.\n..592.....\n......755.\n...$.*....\n.664.598..")

	got := schematic.SumOfPartNumbers()

	if want != got {
		t.Errorf("\ngot : %v\nwant: %v", got, want)
	}
}

func TestGetSymbolIndexes(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  []int
	}{
		{
			name:  "Part 1 first line",
			input: "467..114..",
			want:  nil,
		},
		{
			name:  "Part 1 second line",
			input: "...*......",
			want:  []int{3},
		},
		{
			name:  "Part 1 third line",
			input: "..35..633.",
			want:  nil,
		},
		{
			name:  "Part 1 fourth line",
			input: "......#...",
			want:  []int{6},
		},
		{
			name:  "Part 1 fifth line",
			input: "617*......",
			want:  []int{3},
		},
		{
			name:  "Part 1 sixth line",
			input: ".....+.58.",
			want:  []int{5},
		},
		{
			name:  "Part 1 seventh line",
			input: "..592.....",
			want:  nil,
		},
		{
			name:  "Part 1 eigth line",
			input: "......755.",
			want:  nil,
		},
		{
			name:  "Part 1 nineth line",
			input: "...$.*....",
			want:  []int{3, 5},
		},
		{
			name:  "Part 1 tenth line",
			input: ".664.598..",
			want:  nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetSymbolIndexes(tt.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetSymbolIndexes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetGearRatios(t *testing.T) {
	type args struct {
		numbers []NumberPosition
		input   string
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "",
			args: args{
				numbers: []NumberPosition{
					{
						startIndex: 0,
						endIndex:   3,
						value:      467,
						rowIndex:   0,
					},
					{
						startIndex: 5,
						endIndex:   8,
						value:      114,
						rowIndex:   0,
					},
					{
						startIndex: 2,
						endIndex:   4,
						value:      35,
						rowIndex:   2,
					},
					{
						startIndex: 6,
						endIndex:   9,
						value:      633,
						rowIndex:   2,
					},
					{
						startIndex: 0,
						endIndex:   3,
						value:      617,
						rowIndex:   4,
					},
					{
						startIndex: 7,
						endIndex:   9,
						value:      58,
						rowIndex:   5,
					},
					{
						startIndex: 2,
						endIndex:   5,
						value:      592,
						rowIndex:   6,
					},
					{
						startIndex: 6,
						endIndex:   9,
						value:      755,
						rowIndex:   7,
					},
					{
						startIndex: 1,
						endIndex:   4,
						value:      664,
						rowIndex:   9,
					},
					{
						startIndex: 5,
						endIndex:   8,
						value:      598,
						rowIndex:   9,
					},
				},
				input: "467..114..\n...*......\n..35..633.\n......#...\n617*......\n.....+.58.\n..592.....\n......755.\n...$.*....\n.664.598..",
			},
			want: []int{16345, 451490},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetGearRatios(tt.args.numbers, tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetGearRatios() = %v, want %v", got, tt.want)
			}
		})
	}
}
