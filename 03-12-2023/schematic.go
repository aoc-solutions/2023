package main

import (
	"log"
	"regexp"
	"strings"
)

type Schematic struct {
	partNumbers []int
	gearRatios  []int
}

func GetSchematic(input string) Schematic {
	lines := strings.Split(input, "\n")
	var numbers []NumberPosition
	var symbols = make([][]int, len(lines))

	for i, line := range lines {
		symbols[i] = GetSymbolIndexes(line)
		numbers = append(numbers, GetNumberIndexes(line, i)...)
	}

	var partNumbers []int

	for _, number := range numbers {
		if number.HasNeighbors(symbols) {
			partNumbers = append(partNumbers, number.value)
		}
	}

	return Schematic{
		partNumbers: partNumbers,
		gearRatios:  GetGearRatios(numbers, input),
	}
}

func (schematic Schematic) SumOfPartNumbers() int {
	res := 0

	for _, number := range schematic.partNumbers {
		res += number
	}

	return res
}

func GetSymbolIndexes(input string) []int {
	symbolsRegex := regexp.MustCompile("([^\\\\.\\d])")
	symbolIndexes := symbolsRegex.FindAllStringIndex(input, -1)
	var res []int

	for _, symbol := range symbolIndexes {
		// we're finding single character index, no need to know when it ends
		res = append(res, symbol[0])
	}

	return res
}

func GetGearRatios(numbers []NumberPosition, input string) []int {
	var res []int
	lines := strings.Split(input, "\n")
	gearsRegex := regexp.MustCompile("(\\*)")

	for rowIndex, line := range lines {
		gearIndexes := gearsRegex.FindAllStringIndex(line, -1)
		for _, gearIndex := range gearIndexes {
			indexStart := gearIndex[0]
			var numbersInRange []NumberPosition
			for _, number := range numbers {
				// this really illustrates the shortcomings of this structure.
				// If I had used a numbers [][] it would probably have been easier to do by a long shot and faster
				if number.rowIndex >= rowIndex-1 && number.rowIndex <= rowIndex+1 {
					// Within the correct rows
					if number.startIndex-1 <= indexStart && indexStart <= number.endIndex {
						// within the boundaries
						numbersInRange = append(numbersInRange, number)
					}
				}
			}
			if numbersInRange == nil || len(numbersInRange) < 2 {
				continue
			} else if len(numbersInRange) > 2 {
				log.Fatalf("There cannot be '%d' numbers in range, this logic has an error", len(numbersInRange))
			}

			res = append(res, numbersInRange[0].value*numbersInRange[1].value)
		}
	}

	return res
}

func (schematic Schematic) SumOfGearRatios() int {
	res := 0

	for _, ratio := range schematic.gearRatios {
		res += ratio
	}

	return res
}
