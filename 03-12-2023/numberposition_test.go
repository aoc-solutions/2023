package main

import (
	"reflect"
	"testing"
)

func TestGetNumberIndexes(t *testing.T) {
	tests := []struct {
		name  string
		input string
		want  []NumberPosition
	}{
		{
			name:  "Part 1 first line",
			input: "467..114..",
			want: []NumberPosition{
				{
					startIndex: 0,
					endIndex:   3,
					value:      467,
				},
				{
					startIndex: 5,
					endIndex:   8,
					value:      114,
				},
			},
		},
		{
			name:  "Part 1 second line",
			input: "...*......",
			want:  nil,
		},
		{
			name:  "Part 1 third line",
			input: "..35..633.",
			want: []NumberPosition{
				{
					startIndex: 2,
					endIndex:   4,
					value:      35,
				},
				{
					startIndex: 6,
					endIndex:   9,
					value:      633,
				},
			},
		},
		{
			name:  "Part 1 fourth line",
			input: "......#...",
			want:  nil,
		},
		{
			name:  "Part 1 fifth line",
			input: "617*......",
			want: []NumberPosition{
				{
					startIndex: 0,
					endIndex:   3,
					value:      617,
				},
			},
		},
		{
			name:  "Part 1 sixth line",
			input: ".....+.58.",
			want: []NumberPosition{
				{
					startIndex: 7,
					endIndex:   9,
					value:      58,
				},
			},
		},
		{
			name:  "Part 1 seventh line",
			input: "..592.....",
			want: []NumberPosition{
				{
					startIndex: 2,
					endIndex:   5,
					value:      592,
				},
			},
		},
		{
			name:  "Part 1 eigth line",
			input: "......755.",
			want: []NumberPosition{
				{
					startIndex: 6,
					endIndex:   9,
					value:      755,
				},
			},
		},
		{
			name:  "Part 1 nineth line",
			input: "...$.*....",
			want:  nil,
		},
		{
			name:  "Part 1 tenth line",
			input: ".664.598..",
			want: []NumberPosition{
				{
					startIndex: 1,
					endIndex:   4,
					value:      664,
				},
				{
					startIndex: 5,
					endIndex:   8,
					value:      598,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetNumberIndexes(tt.input, 0); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("\ngot : %v\nwant: %v\n", got, tt.want)
			}
		})
	}
}

func TestNumberPosition_HasNeighbors(t *testing.T) {
	type fields struct {
		startIndex int
		endIndex   int
		rowIndex   int
	}
	type args struct {
		symbolIndexes [][]int
	}
	symbolIndexes := [][]int{
		{},
		{3},
		{},
		{6},
		{3},
		{5},
		{},
		{},
		{3, 5},
		{},
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "On the first row can find symbol diagonally to the right",
			fields: fields{
				startIndex: 0,
				endIndex:   3,
				rowIndex:   0,
			},
			args: args{symbolIndexes: symbolIndexes},
			want: true,
		},
		{
			name: "On the first row, second number has no symbol",
			fields: fields{
				startIndex: 5,
				endIndex:   8,
				rowIndex:   0,
			},
			args: args{symbolIndexes: symbolIndexes},
			want: false,
		},
		{
			name: "On the third row can find symbol on top of it",
			fields: fields{
				startIndex: 2,
				endIndex:   4,
				rowIndex:   2,
			},
			args: args{symbolIndexes: symbolIndexes},
			want: true,
		},
		{
			name: "On the third row can find symbol on bottom of it",
			fields: fields{
				startIndex: 6,
				endIndex:   9,
				rowIndex:   2,
			},
			args: args{symbolIndexes: symbolIndexes},
			want: true,
		},
		{
			name: "On the fifth row can find symbol to the right of it",
			fields: fields{
				startIndex: 0,
				endIndex:   3,
				rowIndex:   4,
			},
			args: args{symbolIndexes: symbolIndexes},
			want: true,
		},
		{
			name: "On the sixth row there is no symbol next to it",
			fields: fields{
				startIndex: 7,
				endIndex:   9,
				rowIndex:   5,
			},
			args: args{symbolIndexes: symbolIndexes},
			want: false,
		},
		{
			name: "On the seventh row there is a symbol diagonally top right of it",
			fields: fields{
				startIndex: 2,
				endIndex:   5,
				rowIndex:   6,
			},
			args: args{symbolIndexes: symbolIndexes},
			want: true,
		},
		{
			name: "On the eighth row there is a symbol diagonally bottom left of it",
			fields: fields{
				startIndex: 6,
				endIndex:   9,
				rowIndex:   7,
			},
			args: args{symbolIndexes: symbolIndexes},
			want: true,
		},
		{
			name: "On the tenth row there is a symbol on top of it",
			fields: fields{
				startIndex: 1,
				endIndex:   4,
				rowIndex:   9,
			},
			args: args{symbolIndexes: symbolIndexes},
			want: true,
		},
		{
			name: "On the tenth row second symbol there is a symbol on top of it",
			fields: fields{
				startIndex: 5,
				endIndex:   8,
				rowIndex:   9,
			},
			args: args{symbolIndexes: symbolIndexes},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			numberPosition := NumberPosition{
				startIndex: tt.fields.startIndex,
				endIndex:   tt.fields.endIndex,
				rowIndex:   tt.fields.rowIndex,
			}
			if got := numberPosition.HasNeighbors(tt.args.symbolIndexes); got != tt.want {
				t.Errorf("HasNeighbors() = %v, want %v", got, tt.want)
			}
		})
	}
}
