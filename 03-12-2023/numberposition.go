package main

import (
	"log"
	"regexp"
	"slices"
	"strconv"
)

type NumberPosition struct {
	startIndex int
	endIndex   int
	rowIndex   int
	value      int
}

func GetNumberIndexes(input string, rowIndex int) []NumberPosition {
	numbersRegex := regexp.MustCompile("\\d+")
	numberIndexes := numbersRegex.FindAllStringIndex(input, -1)
	var res []NumberPosition

	for _, number := range numberIndexes {
		startIndex := number[0]
		endIndex := number[1]
		valueAsString := input[startIndex:endIndex]
		value, err := strconv.Atoi(valueAsString)

		if err != nil {
			log.Fatalf("Failed to convert string representation of value: '%s' with error: '%s'", valueAsString, err)
		}

		res = append(res, NumberPosition{
			startIndex: startIndex,
			endIndex:   endIndex,
			rowIndex:   rowIndex,
			value:      value,
		})
	}

	return res
}

func (numberPosition NumberPosition) HasNeighbors(symbolIndexes [][]int) bool {
	rowIndex := numberPosition.rowIndex
	startIndex := numberPosition.startIndex - 1
	endIndex := numberPosition.endIndex

	if startIndex < 0 {
		startIndex = 0
	}

	// First look on same rowindex as self
	symbolRow := symbolIndexes[rowIndex]
	if slices.Contains(symbolRow, startIndex) || slices.Contains(symbolRow, endIndex) {
		return true
	}

	// Next check previous row
	if rowIndex > 0 {
		previousSymbolRow := symbolIndexes[rowIndex-1]
		for i := startIndex; i < endIndex+1; i++ {
			if slices.Contains(previousSymbolRow, i) {
				return true
			}
		}
	}

	// Finally check the next row
	nextRowIndex := rowIndex + 1
	if nextRowIndex < len(symbolIndexes) {
		nextSymbolRow := symbolIndexes[nextRowIndex]
		for i := startIndex; i < endIndex+1; i++ {
			if slices.Contains(nextSymbolRow, i) {
				return true
			}
		}
	}

	return false
}
