package main

import (
	"fmt"
	"os"
)

func main() {
	inputBytes, err := os.ReadFile("input")
	if err != nil {
		fmt.Printf("Failed to read file with error: '%s'", err)
		os.Exit(1)
	}
	input := string(inputBytes)
	schematic := GetSchematic(input)

	fmt.Printf("Sum of all part numbers: '%d'\n", schematic.SumOfPartNumbers())
	fmt.Printf("Sum of gear ratios: '%d'\n", schematic.SumOfGearRatios())
}
