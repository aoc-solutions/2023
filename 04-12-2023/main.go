package main

import (
	"fmt"
	"os"
)

func main() {
	inputBytes, err := os.ReadFile("input.txt")
	if err != nil {
		fmt.Printf("Failed to read file with error: '%s'", err)
		os.Exit(1)
	}
	input := string(inputBytes)
	scratchCards := ScratchcardsFromString(input)
	fmt.Printf("Total sum of scratchcards: %d\n", SumPoints(scratchCards))
	fmt.Printf("Total sum of duplicates: %d\n", GetAmountOfWinningScratchcardsWithCopies(scratchCards))
}
