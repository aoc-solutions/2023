package main

import (
	"reflect"
	"testing"
)

var day4part1Inputs = []Scratchcard{
	{
		id:             1,
		numbers:        []int{83, 86, 6, 31, 17, 9, 48, 53},
		winningNumbers: []int{41, 48, 83, 86, 17},
	},
	{
		id:             2,
		numbers:        []int{61, 30, 68, 82, 17, 32, 24, 19},
		winningNumbers: []int{13, 32, 20, 16, 61},
	},
	{
		id:             3,
		numbers:        []int{69, 82, 63, 72, 16, 21, 14, 1},
		winningNumbers: []int{1, 21, 53, 59, 44},
	},
	{
		id:             4,
		numbers:        []int{59, 84, 76, 51, 58, 5, 54, 83},
		winningNumbers: []int{41, 92, 73, 84, 69},
	},
	{
		id:             5,
		numbers:        []int{88, 30, 70, 12, 93, 22, 82, 36},
		winningNumbers: []int{87, 83, 26, 28, 32},
	},
	{
		id:             6,
		numbers:        []int{74, 77, 10, 23, 35, 67, 36, 11},
		winningNumbers: []int{31, 18, 13, 56, 72},
	},
}

func TestScratchcard_points(t *testing.T) {
	tests := []struct {
		name   string
		fields Scratchcard
		want   int
	}{
		{
			name:   "Four winning numbers is equal to eight points",
			fields: day4part1Inputs[0],
			want:   8,
		},
		{
			name:   "Two winning numbers is equal to two points",
			fields: day4part1Inputs[1],
			want:   2,
		},
		{
			name:   "Two winning numbers is equal to two points, part 2",
			fields: day4part1Inputs[2],
			want:   2,
		},
		{
			name:   "One winning numbers is equal to one point",
			fields: day4part1Inputs[3],
			want:   1,
		},
		{
			name:   "No winning numbers is equal to zero points",
			fields: day4part1Inputs[4],
			want:   0,
		},
		{
			name:   "No winning numbers is equal to zero points part two",
			fields: day4part1Inputs[5],
			want:   0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			scratchcard := Scratchcard{
				id:             tt.fields.id,
				numbers:        tt.fields.numbers,
				winningNumbers: tt.fields.winningNumbers,
			}
			if got := scratchcard.Points(); got != tt.want {
				t.Errorf("Points() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ScratchcardsFromString(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name string
		args args
		want []Scratchcard
	}{
		{
			name: "Day 1 part 1 input",
			args: args{
				input: "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53\nCard 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19\nCard 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1\nCard 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83\nCard 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36\nCard 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11",
			},
			want: day4part1Inputs,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ScratchcardsFromString(tt.args.input); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ScratchcardsFromString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSumPoints(t *testing.T) {
	tests := []struct {
		name string
		args []Scratchcard
		want int
	}{
		{
			name: "Day 1 part 1 input",
			args: day4part1Inputs,
			want: 13,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SumPoints(tt.args); got != tt.want {
				t.Errorf("SumPoints() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getSumOfDuplicates(t *testing.T) {
	cardsMatches := [][]int{
		{2, 3, 4, 5},
		{3, 4},
		{4, 5},
		{5},
		nil,
		nil,
	}
	type args struct {
		duplicateId  int
		cardsMatches [][]int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Card 1 has four duplicates and should return 15",
			args: args{
				duplicateId:  1,
				cardsMatches: cardsMatches,
			},
			want: 15,
		},
		{
			name: "Card 2 has two duplicates and should return 7",
			args: args{
				duplicateId:  2,
				cardsMatches: cardsMatches,
			},
			want: 7,
		},
		{
			name: "Card 3 has two duplicates and should return 4",
			args: args{
				duplicateId:  3,
				cardsMatches: cardsMatches,
			},
			want: 4,
		},
		{
			name: "Card 4 has one duplicates and should return 2",
			args: args{
				duplicateId:  4,
				cardsMatches: cardsMatches,
			},
			want: 2,
		},
		{
			name: "Card 5 has no duplicates and should return 1",
			args: args{
				duplicateId:  5,
				cardsMatches: cardsMatches,
			},
			want: 1,
		},
		{
			name: "Card 6 has no duplicates and should return 1",
			args: args{
				duplicateId:  6,
				cardsMatches: cardsMatches,
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getSumOfDuplicates(tt.args.duplicateId, tt.args.cardsMatches); got != tt.want {
				t.Errorf("getSumOfDuplicates() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetAmountOfWinningScratchcardsWithCopies(t *testing.T) {
	tests := []struct {
		name string
		args []Scratchcard
		want int
	}{
		{
			name: "Day 4 input",
			args: day4part1Inputs,
			want: 30,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetAmountOfWinningScratchcardsWithCopies(tt.args); got != tt.want {
				t.Errorf("GetAmountOfWinningScratchcardsWithCopies() = %v, want %v", got, tt.want)
			}
		})
	}
}
