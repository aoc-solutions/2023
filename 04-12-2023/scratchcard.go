package main

import (
	"log"
	"math"
	"regexp"
	"slices"
	"strconv"
	"strings"
)

type Scratchcard struct {
	id             int
	numbers        []int
	winningNumbers []int
}

func (scratchcard Scratchcard) Points() int {
	matches := scratchcard.Matches()

	if matches == 0 {
		return 0
	} else {
		return int(math.Pow(2, float64(matches-1)))
	}
}

func (scratchcard Scratchcard) Matches() int {
	var matches int

	for _, number := range scratchcard.numbers {
		if slices.Contains(scratchcard.winningNumbers, number) {
			matches++
		}
	}

	return matches
}

func GetAmountOfWinningScratchcardsWithCopies(scratchcards []Scratchcard) int {
	var res int
	amountOfScratchcards := len(scratchcards)
	cardsMatches := make([][]int, amountOfScratchcards)

	for _, scratchcard := range scratchcards {
		matches := scratchcard.Matches()
		cardIndex := scratchcard.id - 1
		if matches == 0 {
			cardsMatches[cardIndex] = nil
		} else {
			ids := make([]int, matches)
			index := 0
			for value := scratchcard.id + 1; value <= scratchcard.id+matches; value++ {
				ids[index] = value
				index++
			}
			cardsMatches[cardIndex] = ids
		}
	}

	for i := 1; i < amountOfScratchcards+1; i++ {
		res += getSumOfDuplicates(i, cardsMatches)
	}

	return res
}

func getSumOfDuplicates(duplicateId int, cardsMatches [][]int) int {
	duplicateMatchIds := cardsMatches[duplicateId-1]
	sum := 1

	if duplicateMatchIds != nil {
		for _, duplicateMatchId := range duplicateMatchIds {
			sum += getSumOfDuplicates(duplicateMatchId, cardsMatches)
		}
	}

	return sum
}

func SumPoints(scratchcards []Scratchcard) int {
	res := 0

	for _, scratchcard := range scratchcards {
		res += scratchcard.Points()
	}

	return res
}

func toIntSlice(input []string) []int {
	res := make([]int, len(input))

	for i, value := range input {
		valueConv, err := strconv.Atoi(value)
		if err != nil {
			log.Fatalf("Failed to convert '%s' to an integer", value)
		}
		res[i] = valueConv
	}

	return res
}

func ScratchcardsFromString(input string) []Scratchcard {
	lines := strings.Split(input, "\n")
	var res []Scratchcard

	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		res = append(res, scratchcard_from_line(line))
	}

	return res
}

func scratchcard_from_line(line string) Scratchcard {
	idRegex := regexp.MustCompile("\\d+")
	cardData := strings.Split(line, ": ")
	cardId := idRegex.FindString(cardData[0])
	numbers := strings.Replace(cardData[1], "  ", " ", -1)
	id, err := strconv.Atoi(cardId)

	if err != nil {
		log.Fatalf("failed to convert '%s' to integer as the ID of line '%s'", cardId, line)
	}
	// Workaround for code printing single digit numbers with an additional space
	numbersData := strings.Split(numbers, "|")
	winningNumbers := toIntSlice(strings.Split(strings.Trim(numbersData[0], " "), " "))
	yourNumbers := toIntSlice(strings.Split(strings.Trim(numbersData[1], " "), " "))
	return Scratchcard{
		id:             id,
		numbers:        yourNumbers,
		winningNumbers: winningNumbers,
	}
}
